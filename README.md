
# ****React Native Monorepo Template设计文档****

## ****29th June 2023 Vincent****

  

  




  

##  ****背景****

在gamefi新app开发项目中，需要实现组件和模块化需求，最终沉淀输出可复用的组件模块，在开发协作上做到支持不同团队开发。实现app端也能微前端开发部署。

##

##  ****设计****

  

### ****1、利用yarn workspace实现monerepo****

在基于文章<https://todoit.tech/monorepo.html> 基础上，基于最新RN版本0.72.0基础上实现了通用模块common和业务模块module-a和module-b，并通过eslint-plugin-workplace对依赖导入做了严格规范。

  

### ****2、生成react native template****

基于文章<https://dev.to/roycechua/how-to-make-your-own-custom-react-native-templates-2021-20l5>，生成模块项目 <https://gitlab.com/vincent.yang1/react-native-monorepo-template>

  

### ****3、根据模块生成新RN Monorepo App****

在命令行执行以下shell命名

  

npx react-native init SomeApp --template https://gitlab.com/vincent.yang1/react-native-monorepo-template.git

  

生成文件结构如下：

  

  

通过运行android和ios，发现均已实现